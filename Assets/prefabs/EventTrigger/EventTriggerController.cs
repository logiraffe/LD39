﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventTriggerController : MonoBehaviour {
	private PlayerController player;
	private ModalController modal;
	private bool calledIntroMusic = false;
    private bool calledTurtleMusic = false;
    private bool calledWhaleMusic = false;
    private bool calledEndingMusic = false;
    private bool calledEvent1 = false;
	private bool calledEvent2 = false;
	private bool calledEvent3 = false;
	private bool calledEvent4 = false;
	private bool calledEvent5 = false;
	private bool calledEvent6 = false;
	private bool calledEvent7 = false;
	private bool calledEvent8 = false;
	private bool calledEvent9 = false;
    private bool calledEvent10 = false;
    private bool calledEvent11 = false;
    private bool loaded2 = false;
    private bool loaded3 = false;
    private bool killedTheWhales = false;
    private bool killedEverything = false;
    private bool starKilled = false;
    private bool playerKilled = false;
    private bool star_all_gone = false;

//	public GameObject modalObject;

    // Use this for initialization
    void Start () {
		player = FindObjectOfType<PlayerController>();
		modal = FindObjectOfType<ModalController>();

		// Not working. I dunno why, but I guess it's got something to do with you.   //Leonard likes this comment
//		Instantiate(modalObject);
//		GameObject temp = Instantiate(modalObject);
//		temp.name = "Modal";
//		modal = modalObject.GetComponent<ModalController>();

		StartCoroutine(eventOccurence());
        DontDestroyOnLoad(this.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
		if(player == null)
        {
            player = FindObjectOfType<PlayerController>();
        }
        if (modal == null)
        {
            modal = FindObjectOfType<ModalController>();
        }
    }

	IEnumerator eventOccurence()
	{
		while (true) {
			float playerX = player.transform.position.x;

			if (playerX > 10f && !calledIntroMusic && playerX < 250f) { //added third check because I am sick and tired of those same 4 chords playing in world 2
				FindObjectOfType<music_manager>().playIntro();
				calledIntroMusic = true;
			} 
			else if (playerX > 15f && !calledEvent1) {
				modal.show("The outage came without warning. Overnight, the light dimmed for thousands.");
				calledEvent1 = true;
			} 
			else if (playerX > 40f && !calledEvent2) {
				modal.show("The plague was indiscriminate and spread rapidly. A wave of death wiping the ocean floor like a slate.");
				calledEvent2 = true;
			}
			else if (playerX > 75f && !calledEvent3) {
				modal.show("None knew what the light was, but all knew the source. Lodged in the mouth of an abyss - a star of unrivaled brightness, power radiating across the entire sea. The only hope of refuge.");
				calledEvent3 = true;
			}
			else if (playerX > 140f && !calledEvent4) {
				modal.show("As the power ran out, so too did the smaller creatures. Their tiny reservoirs only held so much, and one by one were extinguished.");
				calledEvent4 = true;
			}
            else if (playerX > 252f && playerX < 275f && SceneManager.GetActiveScene().name == "level_2")
            {
                player.transform.Translate(Vector3.forward * 20);
            }
			else if (playerX > 315f && !calledEvent5) {
				modal.show("With more hope than most, the mantas raced ahead of the slower beings, creating eddies of current in their wake.");
				calledEvent5 = true;
			} else if (playerX > 470f && !calledTurtleMusic && playerX < 600f)
            {
                FindObjectOfType<music_manager>().playTurtles();
                calledTurtleMusic = true;
            }
            else if (playerX > 700f && !calledEvent6) {
				modal.show("The shell-beaks persevered in stubbornness. They moved far too slowly to stand a chance against the death. Shells had kept them safe thus far, but defenses mean nothing without power.");
				calledEvent6 = true;
			}
            else if (playerX >880f && !calledWhaleMusic && playerX < 940f)
            {
                FindObjectOfType<music_manager>().playWhales();
                calledWhaleMusic = true;
            }
            else if (playerX > 925f && !calledEvent7) {
				modal.show("Behemoths roamed the vast expanse of big water, storing their reserves of power for months on end. But even they could not escape.");
				calledEvent7 = true;
			}
            else if (playerX > 990f && !killedTheWhales && playerX < 1100f) // Sea World's got nothing on us
            {
                whale[] whales = FindObjectsOfType<whale>();
                foreach (whale whall_e in whales)
                {
                    // LET THE BODIES HIT THE ~~FLOOR~~ INFINITE VOID BENEATH US
                    whall_e.GetComponent<AnimalInterface>().killMe(Random.Range(3.0f, 7.0f));
                }
                killedTheWhales = true;
            }
			
            else if (playerX > 1080f && !calledEndingMusic && playerX < 1200f)
            {
                FindObjectOfType<music_manager>().playEnd();
                calledEndingMusic = true;
            }
            else if (playerX > 1040f && !calledEvent8 && playerX < 1100f)
            {
                modal.show("Those remaining in the exodus converged on hope. Every creature with light left had fled to take shelter in the source of life.");
                calledEvent8 = true;
            }
            else if (playerX > 1090f && !calledEvent9)
            {
                modal.show("Hundreds slipped into the abyss as the wave overtook them. One by one they went dark.");
                calledEvent9 = true;
            }
            else if (playerX > 1150f && !calledEvent10 && playerX < 1200f)
            {
                modal.show("As their bodies faded black and lifeless as coal, the life source neared. It too shuddered under the weight of disease, and began to fade");
                calledEvent10 = true;
            }
            else if (playerX > 1190f && !starKilled && playerX < 1290f)
            {
                FindObjectOfType<powerSource>().killMe(10.0f);
                yield return new WaitForSeconds(5.0f);
                FindObjectOfType<backgroundKiller>().killMe(10.0f);
                yield return new WaitForSeconds(3.0f);
                player.GetComponent<PlayerAesthetics>().fadeTo(0.0f, 10.0f);
                yield return new WaitForSeconds(10.0f);
                starKilled = true;
            } else if (starKilled && !playerKilled && !GameObject.Find("music_AS").GetComponent<AudioSource>().isPlaying)
            {
                playerKilled = true;
                DontDestroyOnLoad(this.gameObject);
                SceneManager.LoadScene("MainMenu");
                yield return null;
                FindObjectOfType<MainMenuController>().thisBitchDontKnowBoutPangea();
                Destroy(FindObjectOfType<PlayerController>().gameObject);
                Destroy(this.gameObject);
            }

            yield return new WaitForSeconds(0.5f); // check every now and then
		}
	}
    
}
