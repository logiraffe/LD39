﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backgroundKiller : MonoBehaviour {

    public ParticleSystem caustics;
    public SpriteRenderer background;

    public SpriteRenderer background_piece_A, background_piece_B;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
	}


    public void killMe(float fadeTime)
    {
        StartCoroutine(fadeBG(fadeTime));
        StartCoroutine(killCaustics(fadeTime));
    }

    IEnumerator fadeBG(float fadeTime)
    {
        float elapsed = 0.0f;
        Color oldColor = background.color;

        //Hack because I'm out of time.  These two pieces of background are still visible at the end when we call this.  Dim them too.
        Color bgA = background_piece_A.color;
        Color bgB = background_piece_B.color;
        
        while(elapsed < fadeTime)
        {
            background.color = Color.Lerp(oldColor, Color.black, (elapsed / fadeTime));
            background_piece_A.color = Color.Lerp(bgA, Color.black, (elapsed / fadeTime));
            background_piece_B.color = Color.Lerp(bgA, Color.black, (elapsed / fadeTime));
            elapsed += Time.deltaTime;
            yield return null;
        }

        background.color = Color.black;
        background_piece_A.color = Color.black;
        background_piece_B.color = Color.black;
        yield return null;
    }

    IEnumerator killCaustics(float fadeTime)
    {
        float elapsed = 0.0f;
        fadeTime /= 5;

        ParticleSystem.EmissionModule temp = caustics.emission;
        float initial = temp.rateOverTimeMultiplier;
        while (elapsed < fadeTime)
        {
            temp.rateOverTimeMultiplier = Mathf.Lerp(initial, 0, (elapsed / fadeTime));

            elapsed += Time.deltaTime;
            yield return null;
        }

        temp.rateOverTimeMultiplier = 0.0f;
        
        yield return null;
    }

}
