﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface AnimalInterface {

    void setSwimDirection(float x, float y);
    void setSwimSpeed(float speed);
    void randomizeSize();
    void randomize();
    void killMe(float deathTime);
    float spawnWait();
    //void happyDance();
}
