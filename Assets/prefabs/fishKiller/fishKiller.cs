﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fishKiller : MonoBehaviour
{

    public FishSpawner.creatureTypes killType;
    public bool all_at_once;
    public float time_between_deaths;

    private PlayerController player;
    private bool jobDone;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        jobDone = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!jobDone && player.transform.position.x > this.transform.position.x && (player.transform.position.x - this.transform.position.x) < 30)
        {
            switch (killType)
            {
                case FishSpawner.creatureTypes.lumi_fish:
                case FishSpawner.creatureTypes.angel_fish:
                    StartCoroutine(killFish());
                    break;
                case FishSpawner.creatureTypes.ray:
                case FishSpawner.creatureTypes.siderays:
                    StartCoroutine(killRays());
                    break;
                case FishSpawner.creatureTypes.turtle:
                    StartCoroutine(killTurtles());
                    break;
                case FishSpawner.creatureTypes.whale:
                    StartCoroutine(killWhales());
                    break;
                default:
                    StartCoroutine(killFish());
                    break;
            }

            jobDone = true;
        }
    }


    public IEnumerator killFish()
    {
        Fish[] fishes = FindObjectsOfType<Fish>();
        foreach (Fish fish in fishes) //huh.  Never hit that pluralization problem before
        {
            fish.GetComponent<AnimalInterface>().killMe(Random.Range(2.0f, 4.0f));
            if (!all_at_once)
            {
                yield return new WaitForSeconds(time_between_deaths + Random.Range(-1.0f, 1.0f));
            }
        }
    }

    public IEnumerator killRays()
    {
        ray[] mantas = FindObjectsOfType<ray>();
        foreach (ray manta in mantas)
        {
            manta.GetComponent<AnimalInterface>().killMe(Random.Range(2.0f, 4.0f));
            if (!all_at_once)
            {
                yield return new WaitForSeconds(time_between_deaths + Random.Range(-1.0f, 1.0f));
            }
        }
    }

    public IEnumerator killTurtles()
    {
        turtle[] turtles = FindObjectsOfType<turtle>();
        foreach (turtle turt in turtles)
        {
            turt.GetComponent<AnimalInterface>().killMe(Random.Range(2.0f, 4.0f));
            if (!all_at_once)
            {
                yield return new WaitForSeconds(time_between_deaths + Random.Range(-1.0f, 1.0f));
            }
        }
    }

    public IEnumerator killWhales()
    {
        whale[] whales = FindObjectsOfType<whale>();
        foreach (whale whal_e in whales)
        {
            whal_e.GetComponent<AnimalInterface>().killMe(Random.Range(2.0f, 4.0f));
            if (!all_at_once)
            {
                yield return new WaitForSeconds(time_between_deaths + Random.Range(-1.0f, 1.0f));
            }
        }
    }



    void OnDrawGizmos()
    {

        switch (killType)
        {
            case FishSpawner.creatureTypes.lumi_fish:
            case FishSpawner.creatureTypes.angel_fish:
                Gizmos.DrawIcon(transform.position, "kill_fish.png", true);
                break;
            case FishSpawner.creatureTypes.ray:
            case FishSpawner.creatureTypes.siderays:
                Gizmos.DrawIcon(transform.position, "kill_ray.png", true);
                break;
            case FishSpawner.creatureTypes.turtle:
                Gizmos.DrawIcon(transform.position, "kill_turtle.png", true);
                break;
            case FishSpawner.creatureTypes.whale:
                Gizmos.DrawIcon(transform.position, "kill_whale.png", true);
                break;
            default:
                Gizmos.DrawIcon(transform.position, "kill_fish.png", true);
                break;
        }
        Gizmos.color = Color.red;
        


    }
}
