﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fishTrigger : MonoBehaviour {

    public MarinersGuide.spawns call;
    public float manual_speed = 9999;

    private PlayerController player;
    private bool jobDone;

    // Use this for initialization
    void Start () {
        player = FindObjectOfType<PlayerController>();
        jobDone = false;
        if(manual_speed == 0)
        {
            manual_speed = 9999;
        }
	}
	
	// Update is called once per frame
	void Update () {
		if( !jobDone && player.transform.position.x > this.transform.position.x  && (player.transform.position.x - this.transform.position.x) < 30)
        {
            FindObjectOfType<MarinersGuide>().runCall(call, manual_speed);
            jobDone = true;
        }
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawIcon(transform.position, "exc_part.png", true);
    }
}
