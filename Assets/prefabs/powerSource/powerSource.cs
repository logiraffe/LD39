﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerSource : MonoBehaviour {

    public ParticleSystem starAesthetics;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       
	}

    public void killMe(float timeToKill = 3.0f)
    {
        StartCoroutine(fadeAway(timeToKill));
    }

    public IEnumerator fadeAway(float timeToFade)
    {
        float elapsed = 0.0f;
        float startEmitRate = starAesthetics.emission.rateOverTimeMultiplier;
        while (elapsed < timeToFade)
        {
            ParticleSystem.EmissionModule temp = starAesthetics.emission;
            temp.rateOverTimeMultiplier = Mathf.Lerp(startEmitRate, 0.0f, (elapsed / timeToFade));

            elapsed += Time.deltaTime;
            yield return null;
        }
    }
}
