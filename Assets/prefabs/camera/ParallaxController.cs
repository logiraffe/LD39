﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ParallaxController : MonoBehaviour {

	public float offset;
	public GameObject camera;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		this.transform.position = new Vector3(camera.transform.position.x * offset, this.transform.position.y, this.transform.position.z);
	}
}
