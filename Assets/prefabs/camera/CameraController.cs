﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour {

	private Vector2 velocity;
	public float smoothTimeX;
	public GameObject player;


	// Use this for initialization
	void Start () {
        player = FindObjectOfType<PlayerController>().gameObject;
	}
	
	// Update is called once per frame
	void Update () {

        if (player == null)
        {
            player = FindObjectOfType<PlayerController>().gameObject;
        }

        if (this.transform.position.x > 254f && this.transform.position.x < 274f && SceneManager.GetActiveScene().name == "level_1" )
        {
            SceneManager.LoadScene("level_2");
        }

        if (this.transform.position.x > 655f && this.transform.position.x < 690f && SceneManager.GetActiveScene().name == "level_2")
        {
            SceneManager.LoadScene("level_3");
        }

        float posX = Mathf.SmoothDamp(transform.position.x, player.transform.position.x + 5f, ref velocity.x, smoothTimeX);

		if (posX > this.transform.position.x && posX <= 1200.0f) {
			this.transform.position = new Vector3(posX, transform.position.y, transform.position.z);
		}
	}

	public void moveRight(float moveAmountX) {
		float spoothX = Mathf.SmoothDamp(transform.position.x, transform.position.x + moveAmountX, ref velocity.x, smoothTimeX);
        this.transform.position = new Vector3(spoothX, transform.position.y, transform.position.z);
	}
}
