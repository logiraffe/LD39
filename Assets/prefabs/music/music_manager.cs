﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music_manager : MonoBehaviour {

    public AudioSource music;

    public AudioClip shawn_learned_garage_band_today;
    public AudioClip turtle_tracks;
    public AudioClip whale_its_about_time;
    public AudioClip death_loop;
    public AudioClip death_end;
    public AudioClip death;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void playIntro()
    {
        music.clip = shawn_learned_garage_band_today;
        //music.loop = true;
        music.Play();
    }

    public void playTurtles()
    {
        music.clip = turtle_tracks;
        music.loop = false;
        music.Play();
    }

    public void playWhales()
    {
        Debug.Log("Here's your music, sir");
        music.clip = whale_its_about_time;
        music.loop = false;
        music.Play();
    }

    public void beginDeath()
    {
        //play on loop
    }

    public void endDeath()
    {
        //wait until current track finishes before starting up
    }

    public void playEnd()
    {
        music.clip = death;
        music.loop = false;
        music.Play();
    }



}
