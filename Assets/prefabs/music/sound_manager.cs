﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound_manager : MonoBehaviour {

    public AudioSource sound;

    public AudioClip[] food;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void playFood()
    {
        sound.clip = food[Random.Range(0, food.Length)];
        sound.Play();
    }
}
