﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarinersGuide : MonoBehaviour {

    // SHAWN,
    // DO NOT REMOVE ANY ENTRIES FROM THIS LIST.  
    // ALL PREVIOUSLY MADE SPAWNERS GET RESET
    //                   -PAST SHAWN

    public enum spawns
    {
        // Fish
        solo_fish_from_left,
        school_of_fish_from_left,
        solo_fish_from_right,
        school_of_fish_from_right,
        solo_fish_from_top,
        school_of_fish_from_top,
        solo_fish_near_screen,
        school_of_fish_near_screen,
        solo_fish_in_distance,
        school_of_fish_in_distance,
        school_of_fish_all_around,

        // Rays
        solo_ray_from_left,
        solo_ray_from_top_left,
        group_of_rays_from_top_left,
        group_of_rays_from_top,
        group_of_rays_from_left,
        solo_ray_near_screen,
        group_of_rays_near_screen,
        solo_ray_in_distance,
        group_of_rays_in_distance_from_top_left,

        // Turtles
        solo_turtle_from_left,
        solo_turtle_from_right,
        solo_turtle_from_top,
        solo_turtle_from_bottom,
        group_of_turtles_from_top_right,
        group_of_turtles_from_top_left,
        group_of_turtles_from_top,
        group_of_turtles_from_right_everywhere,
        group_of_turtles_from_right,
        solo_turtle_near_screen,
        group_of_turtles_near_screen,
        solo_turtle_in_distance,
        group_of_turtles_in_distance_from_right,
        group_of_turtles_in_distance_from_bottom_right,

        // Whales
        solo_whale_from_left,
        solo_whale_from_right,
        solo_whale_from_top,
        solo_whale_from_bottom,
        group_of_whales_from_top_right,
        group_of_whales_from_top_left,
        group_of_whales_from_top,
        group_of_whales_from_left,
        group_of_whales_from_right,
        solo_whale_near_screen,
        group_of_whales_near_screen,
        solo_whale_in_distance_from_bottom,
        solo_whale_in_distance_from_right,
        group_of_whales_in_distance_from_right,
        group_of_whales_in_distance_from_bottom_right,

        // Varieties
        mixed_rays_fish_from_bottom,
        mixed_rays_fish_from_left,
        mixed_rays_fish_from_top_left,
        mixed_rays_fish_distant,
        mixed_turtles_fish_from_top_left,
        mixed_turtles_fish_from_bottom_left,
        mixed_turtles_fish_from_left,
        mixed_rays_turtles_fish_from_left,
        mixed_rays_turtles_fish_from_right,

        // Events
        first_fish,
        first_ray,
        first_turtle,
        first_whale,
        dead_turtle,
        any_dead_turtle,
        grouped_dead_turtles,
        dead_whale,
        grouped_dead_whales,
        final_calvacade_right,
        gigantic_fish_school_left //possibly good for transition help when reaching a wall

    }



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Modifiers shouldn't need to be used, but just in case they're accounted for
    public GameObject[] triggerTurtle(string turtleCall, float speedModifier = 1.0f, float sizeModifier = 1.0f)
    {
        return null;
    }





    public List<GameObject> runCall(spawns spawnCall, float speedOverride = 9999)
    {
        FishSpawner roe = FindObjectOfType<FishSpawner>();
        List<GameObject> fish = new List<GameObject>(); //no longer necessary but not enough time to remove

        switch (spawnCall)
        {
            case spawns.solo_fish_from_left:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.lumi_fish, "left", false, Random.Range(0, 5000), true, speedOverride, 1, 0));
                break;
            case spawns.school_of_fish_from_left:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.lumi_fish, Random.Range(4, 12), "left", false, false, speedOverride, new float[2] { 1.0f, 0 }, 2.5f, new float[2] { 1.0f, 0.1f }));
                break;
            case spawns.solo_fish_from_right:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.lumi_fish, "left", false, Random.Range(0, 5000), true, speedOverride, 1, 0));
                break;
            case spawns.school_of_fish_from_right:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.lumi_fish, Random.Range(4, 12), "right", false, false, speedOverride, new float[2] { 1.0f, 0 }, 2.5f, new float[2] { 1.0f, 0.1f }));
                break;
            case spawns.solo_fish_from_top:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.lumi_fish, "top", false, Random.Range(0, 5000), true, speedOverride, 0.5f, -0.7f));
                break;
            case spawns.school_of_fish_from_top:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.lumi_fish, Random.Range(4, 12), "top", false, false, speedOverride, new float[2] { 1.0f, -0.6f }, 2.5f, new float[2] { 1.0f, 0.1f }));
                break;
            case spawns.solo_fish_near_screen:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.lumi_fish, "left", false, Random.Range(0, 5000), true, speedOverride, 1, 0, 5.0f, true));
                break;
            case spawns.school_of_fish_near_screen:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.lumi_fish, Random.Range(4, 12), "left", false, false, speedOverride, new float[2] { 1.0f, 0 }, 2.5f, new float[2] { 1.0f, 0.1f }, 5.0f, true));
                break;
            case spawns.solo_fish_in_distance:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.lumi_fish, "left", true, Random.Range(0, 5000), true, speedOverride, 1, 0, 0.7f));
                break;
            case spawns.school_of_fish_in_distance:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.lumi_fish, Random.Range(6, 12), "left", false, true, speedOverride, new float[2] { 1.0f, 0 }, 2.5f, new float[2] { 1.0f, 0.1f }, 0.7f));
                break;
            case spawns.school_of_fish_all_around:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.lumi_fish, Random.Range(12, 24), "left", true, false, speedOverride, new float[2] { 1.0f, 0.0f }, 2.5f, new float[2] { 0.0f, 0.3f }));
                break;

            // Rays
            case spawns.solo_ray_from_left:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.ray, "left", false, Random.Range(0, 5000), true, speedOverride, 1, Random.Range(-0.1f, 0.1f)));
                break;
            case spawns.solo_ray_from_top_left:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.ray, "A", false, Random.Range(0, 5000), true, speedOverride, 1, -1 * Random.Range(0.4f, 0.5f)));
                break;
            case spawns.group_of_rays_from_top_left:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.ray, Random.Range(3, 6), "A", false, false, speedOverride, new float[2] { 1.0f, -0.5f }, 3.5f, new float[2] { 0.2f, 0.2f }));
                break;
            case spawns.group_of_rays_from_top:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.ray, Random.Range(5, 8), "top", true, false, speedOverride, new float[2] { 0.8f, -0.6f }, 2.5f, new float[2] { 0.2f, 0.1f }));
                break;
            case spawns.group_of_rays_from_left:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.ray, Random.Range(3, 8), "left", false, false, speedOverride, new float[2] { 1.0f, 0.0f }, 2.5f, new float[2] { 0.2f, 0.1f }));
                break;
            case spawns.solo_ray_near_screen:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.ray, "left", false, Random.Range(0, 5000), true, speedOverride, 1, Random.Range(-0.1f, 0.1f), 3.0f, true));
                break;
            case spawns.group_of_rays_near_screen:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.ray, Random.Range(2, 5), "left", false, false, speedOverride, new float[2] { 1.0f, 0.0f }, 2.5f, new float[2] { 0.2f, 0.1f }, 1, true));
                break;
            case spawns.solo_ray_in_distance:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.ray, "left", true, Random.Range(0, 5000), true, speedOverride, 1, Random.Range(-0.1f, 0.1f), 0.6f));
                break;
            case spawns.group_of_rays_in_distance_from_top_left:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.ray, Random.Range(3, 6), "A", false, true, speedOverride, new float[2] { 1.0f, -0.5f }, 3.5f, new float[2] { 0.2f, 0.2f }, 0.6f));
                break;

            case spawns.solo_turtle_from_right:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.turtle, "right", false, Random.Range(0, 5000), true, speedOverride, 1, Random.Range(-0.1f, 0.1f)));
                break;
            case spawns.solo_turtle_from_top:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.turtle, "top", false, Random.Range(0, 5000), true, speedOverride, 0.4f, -1 *Random.Range(0.4f, 0.2f)));
                break;
            case spawns.solo_turtle_from_bottom:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.turtle, "bottom", false, Random.Range(0, 5000), true, speedOverride, 1, Random.Range(0.2f, 0.6f)));
                break;
            case spawns.group_of_turtles_from_top_right:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.turtle, "D", false, Random.Range(0, 5000), true, speedOverride, 1, -1 * Random.Range(0.2f, 0.5f)));
                break;
            case spawns.group_of_turtles_from_top_left:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.turtle, Random.Range(2, 5), "1", true, false, speedOverride, new float[2] { 1.0f, -0.2f }, 1.5f, new float[2] { 0.2f, 0.2f }));
                break;
            case spawns.group_of_turtles_from_top:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.turtle, Random.Range(2, 5), "top", true, false, speedOverride, new float[2] { 1.0f, 0.0f }, 1.5f, new float[2] { 0.2f, 0.1f }));
                break;
            case spawns.group_of_turtles_from_right_everywhere:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.turtle, Random.Range(2, 5), "right", true, false, speedOverride, new float[2] { 1.0f, 0.0f }, 1.5f, new float[2] { 0.2f, 0.1f }));
                break;
            case spawns.group_of_turtles_from_right:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.turtle, Random.Range(2, 5), "right", false, false, speedOverride, new float[2] { 1.0f, 0.0f }, 1.5f, new float[2] { 0.2f, 0.1f }));
                break;
            case spawns.solo_turtle_near_screen:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.turtle, "top", false, Random.Range(0, 5000), true, speedOverride, 0.8f, -1 * Random.Range(0.1f, 0.7f), 2.0f, true));
                break;
            case spawns.group_of_turtles_near_screen:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.turtle, Random.Range(2, 5), "top", false, false, speedOverride, new float[2] { 1.0f, 0.0f }, 1.5f, new float[2] { 0.2f, 0.1f }, 2.0f, true));
                break;
            case spawns.solo_turtle_in_distance:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.turtle, "right", true, Random.Range(0, 5000), true, speedOverride, 1, Random.Range(-0.1f, 0.1f), 0.7f));
                break;
            case spawns.group_of_turtles_in_distance_from_right:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.turtle, Random.Range(2, 5), "right", false, true, speedOverride, new float[2] { 1.0f, 0.0f }, 1.5f, new float[2] { 0.2f, 0.1f }));
                break;
            case spawns.group_of_turtles_in_distance_from_bottom_right:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.turtle, Random.Range(2, 5), "H", false, true, speedOverride, new float[2] { 1.0f, 0.0f }, 1.5f, new float[2] { 0.2f, 0.1f }));
                break;

            case spawns.solo_whale_from_left:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.whale, "left", false, Random.Range(0, 5000), true, speedOverride, 1, Random.Range(-0.1f, 0.1f)));
                break;
            case spawns.solo_whale_from_right:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.whale, "right", false, Random.Range(0, 5000), true, speedOverride, 1, Random.Range(-0.1f, 0.1f)));
                break;
            case spawns.solo_whale_from_top:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.whale, "top", false, Random.Range(0, 5000), true, speedOverride, 1, -1 * Random.Range(0.2f, 0.3f)));
                break;
            case spawns.solo_whale_from_bottom:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.whale, "bottom", false, Random.Range(0, 5000), true, speedOverride, 1, Random.Range(0.2f, 0.3f)));
                break;
            case spawns.group_of_whales_from_top_right:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.whale, Random.Range(2, 3), "5", true, false, speedOverride, new float[2] { 1.0f, -0.1f }, 1.5f, new float[2] { 0.2f, 0.1f }));
                break;
            case spawns.group_of_whales_from_top_left:
            case spawns.group_of_whales_from_top:
            case spawns.group_of_whales_from_left:
            case spawns.group_of_whales_from_right:
            case spawns.solo_whale_near_screen:
            case spawns.group_of_whales_near_screen:
            case spawns.solo_whale_in_distance_from_bottom:
            case spawns.solo_whale_in_distance_from_right:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.whale, "right", true, Random.Range(0, 5000), true, speedOverride, 1, Random.Range(-0.1f, 0.1f)));
                break;
            case spawns.group_of_whales_in_distance_from_right:
            case spawns.group_of_whales_in_distance_from_bottom_right:

            case spawns.first_fish:
            case spawns.first_ray:
            case spawns.first_turtle:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.turtle, "E", false, Random.Range(0, 5000), false, speedOverride, 1, Random.Range(0.2f, 0.3f)));
                break;
            case spawns.first_whale:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.whale, "D", true, Random.Range(0, 5000), true, speedOverride, 1, -1 * Random.Range(0.2f, 0.3f), 1));
                break;
            case spawns.dead_turtle:
                GameObject killIt = roe.spawn(FishSpawner.creatureTypes.turtle, "5", false, Random.Range(0, 5000), false, speedOverride, 1, 0);
                killIt.GetComponent<AnimalInterface>().killMe(8.0f);
                break;
            case spawns.any_dead_turtle:
                GameObject killAgain = roe.spawn(FishSpawner.creatureTypes.turtle, "right", false, Random.Range(0, 5000), false, speedOverride, 1, 0);
                killAgain.GetComponent<AnimalInterface>().killMe(7.0f);
                break;
            case spawns.grouped_dead_turtles:
                StartCoroutine(roe.spawnSchool(FishSpawner.creatureTypes.turtle, Random.Range(3, 5), "6", true, false, speedOverride, new float[2] { 1.0f, 0.0f }, 1.0f, new float[2] { 0.2f, 0.1f }, 0.8f));
                StartCoroutine(killAllTurtles());
                break;
            case spawns.dead_whale:
                fish.Add(roe.spawn(FishSpawner.creatureTypes.whale, "6", false, Random.Range(0, 5000), true, speedOverride, 1, 0));
                break;
            case spawns.final_calvacade_right:
            case spawns.gigantic_fish_school_left:

            default:
                // Seriously?  I give you ALL of that and you still hit nothing?
                return null;
        }

        //Get back that list, in case you need it (now rendered pointless because async)
        return fish;
    }

    /* -------------------
              ___
         ,,  // \\
        (_,\/ \_/ \
          \ \_/_\_/>
          /_/  /_/

    ---------------------*/

    // Officially the darkest method I have ever made
    public IEnumerator killAllTurtles()
    {
        yield return new WaitForSeconds(5.0f);

        // Wait why is this a coroutine?
        // Well, to keep the animations from being synchronous between all
        // critters of the same species, I had to introduce spawn delays when
        // creating flocks/schools/murders.
        // With the average turtle wait time between spawns at .6 seconds, 
        // I figure a good 4 seconds probably lets them all clear out
        // before grabbing the full list and slaughtering them.
        // Because async.
        // I am a Javscript programmer at my real job.
        // And yet asynchronous tasks are still the bane of my
        // existence.

        // So short answer: It is a coroutine because of a multi-problem
        // chain deriving from Shawn being too lazy to offset an animation.

        turtle[] turtles = FindObjectsOfType<turtle>();
        foreach(turtle turt in turtles)
        {
            // LET THE BODIES HIT THE ~~FLOOR~~ INFINITE VOID BENEATH US
            turt.GetComponent<AnimalInterface>().killMe(Random.Range(6.0f, 12.0f));
        }

        // Shawn looks up from the corner in a daze.
        // The look on his face says it all.  He remembers nothing.
        // He doesn't know where he is, much less what he's done.
        // None of the atrocities just committed against terrapin-kind
        // can be recalled.  But the fragments of shattered shells littering
        // the floor say everything.  This is the face of a broken man. Writing
        // multithreaded murder methods at midnight.

        // And being entirely too proud of that alliteration.
    }

}
