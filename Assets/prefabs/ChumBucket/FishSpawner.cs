﻿using Anima2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSpawner : MonoBehaviour {

    public GameObject[] angel_fish;
    public GameObject[] lumi_fish;
    public GameObject turtle;
    public GameObject ray;
    public GameObject whale;

    public GameObject[] spawns_top;
    public GameObject[] spawns_left;
    public GameObject[] spawns_right;
    public GameObject[] spawns_bottom;


    public enum creatureTypes {
        angel_fish,
        lumi_fish,
        ray,
        siderays,
        turtle,
        whale
    };

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
	}


    // Shawn is tired and coding garbage methods.  I am so sorry
    public IEnumerator spawnSchool(creatureTypes creature, int quantity, string side, bool engulfed = false, bool blurry = false, float speed = 9999, float[] direction = null, float speedVariation = 0.0f, float[] directionVariation = null, float sizeModifier = 1.0f, bool close = false)
    {
        GameObject[] spawns = new GameObject[quantity];

        if(speed == 9999)
        {
            speedVariation = 0;
        }

        // 2 bool 4 school
        bool shawnWithoutGlasses = blurry ? true : false;

        for (int i = 0; i < quantity; i++)
        {
            string loc = getSpawnerFromSide(side);

            if (engulfed) {
               shawnWithoutGlasses = Random.Range(0, 2) == 0 ? true : false;
            }

            if (direction != null) //Spawn swimming in the general direction they said to
            {
                float x = direction[0];
                float y = direction[1];

                if (directionVariation != null)
                {
                    x += ( Random.Range(0, directionVariation[0]) - (directionVariation[0] / 2) );
                    y += ( Random.Range(0, directionVariation[1]) - (directionVariation[1] / 2) );
                }

                spawns[i] = spawn(creature, loc, shawnWithoutGlasses, 4000, true, 0, x, y, sizeModifier, close);
            }
            else //No directions specified
            {
                spawns[i] = spawn(creature, loc, shawnWithoutGlasses, 4000, true, 0, 1, 0, sizeModifier, close );
            }

            yield return new WaitForSeconds(spawns[i].GetComponent<AnimalInterface>().spawnWait());
            
        }

        //release all
        foreach(GameObject critter in spawns)
        {
            critter.GetComponent<AnimalInterface>().setSwimSpeed(speed + (Random.Range(0, speedVariation) - (speedVariation / 2)));
        }

        yield return null;
    }

    public GameObject spawn(creatureTypes creature, string location, bool blurry, int sortOffset, bool randSize = true, float speed = 9999, float xDir = 1.0f, float yDir = 0.0f, float sizeOverride = 1.0f, bool closeToCam = false)
    {
        GameObject sushi = getCreature(creature);
        GameObject restaurant = getSpawner(location);
        Vector3 creatureOffset = getOffset(creature, location);
        //instantiate
        GameObject nigiri = Instantiate(sushi, restaurant.transform.position + creatureOffset, Quaternion.identity);

        if (randSize && sizeOverride == 1.0f)
        {
            nigiri.GetComponent<AnimalInterface>().randomizeSize();
        } else if (sizeOverride != 1.0f)
        {
            nigiri.transform.localScale *= sizeOverride;
        }

        nigiri.GetComponent<AnimalInterface>().setSwimDirection(xDir, yDir);

        if (speed != 9999) //boy that is hacky
        {
            nigiri.GetComponent<AnimalInterface>().setSwimSpeed(speed);
        }

        //alter
        return changeLayerData(nigiri, blurry, sortOffset, closeToCam);
    }

    private GameObject changeLayerData(GameObject fish, bool blurry, int sort_offset, bool closeToCam)
    {
        //Apply sorting offset
        foreach(Transform part in fish.transform)
        {
            //ensure z value is 0 because something I do ALWAYS gives me randomly offset values
            part.position = new Vector3(part.position.x, part.position.y, 0);


            SpriteMeshInstance anima_sprite = part.GetComponent<SpriteMeshInstance>();
            SpriteRenderer unity_sprite = part.GetComponent<SpriteRenderer>();
            if (anima_sprite){
                anima_sprite.sortingOrder = anima_sprite.sortingOrder + sort_offset;
            } else if (unity_sprite) {
                unity_sprite.sortingOrder = unity_sprite.sortingOrder + sort_offset;
            } else {
                //Not a sprite I guess?  May need to account for particles if we spawn bubbles this way
            }
        }

        //Apply blur
        if (blurry)
        {
            ChangeLayersRecursively(fish.transform, "blurred_sill");
        }
        if (closeToCam)
        {
            ChangeSortingLayersRecursively(fish.transform, "close");
        }

        return fish;
    }

    private GameObject getCreature(creatureTypes critter)
    {
        switch (critter)
        {
            case creatureTypes.lumi_fish:
                return lumi_fish[Random.Range(0, lumi_fish.Length)];
            case creatureTypes.angel_fish:
                return angel_fish[Random.Range(0, angel_fish.Length)];
            case creatureTypes.ray:
                return ray;
            case creatureTypes.siderays:
                return ray;
            case creatureTypes.turtle:
                return turtle;
            case creatureTypes.whale:
                return whale;
            default:
                return null;
        }
    }

    public GameObject getSpawner(string spawn_point)
    {
        switch (spawn_point)
        {
            case "A":
            case "a":
                return spawns_top[0];
            case "B":
            case "b":
                return spawns_top[1];
            case "C":
            case "c":
                return spawns_top[2];
            case "D":
            case "d":
                return spawns_top[3];

            case "E":
            case "e":
                return spawns_bottom[0];
            case "F":
            case "f":
                return spawns_bottom[1];
            case "G":
            case "g":
                return spawns_bottom[2];
            case "H":
            case "h":
                return spawns_bottom[3];

            case "1":
                return spawns_left[0];
            case "2":
                return spawns_left[1];
            case "3":
                return spawns_left[2];
            case "4":
                return spawns_left[3];

            case "5":
                return spawns_right[0];
            case "6":
                return spawns_right[1];
            case "7":
                return spawns_right[2];
            case "8":
                return spawns_right[3];

            case "top":
                return spawns_top[Random.Range(0, spawns_top.Length)];
            case "left":
                return spawns_left[Random.Range(0, spawns_left.Length)];
            case "right":
                return spawns_right[Random.Range(0, spawns_right.Length)];
            case "bottom":
                return spawns_bottom[Random.Range(0, spawns_bottom.Length)];

            default:
                Debug.LogError("We're gonna need a bigger boat");
                return null;
        }
    }

    // Offset spawning so things aren't popping into view just because they're bigger
    Vector3 getOffset(creatureTypes species, string location)
    {
        float side, top = 0;

        //multiply resulting values by size maybe?  That seems accurate but sleep-deprived brain can't math

        switch (species)
        {
            case creatureTypes.whale:
                side = 5.0f;
                top = 3.0f;
                break;
            case creatureTypes.turtle:
                side = 1.0f;
                top = 1.0f;
                break;
            case creatureTypes.ray:
                side = 1.0f;
                top = 0.0f;
                break;
            default:
                side = 0;
                top = 0;
                break;
        }

        switch (location)
        {
            case "A":
            case "a":
            case "B":
            case "b":
            case "C":
            case "c":
            case "D":
            case "d":
            case "top":
                return new Vector3(0, top, 0);
            case "E":
            case "e":
            case "F":
            case "f":
            case "G":
            case "g":
            case "H":
            case "h":
            case "bottom":
                return new Vector3(0, -1 * top, 0);
            case "1":
            case "2":
            case "3":
            case "4":
            case "left":
                return new Vector3(-1 * side, 0, 0);
            case "5":
            case "6":
            case "7":
            case "8":
            case "right":
                return new Vector3(side, 0, 0);
            default:
                return new Vector3(0, 0, 0);
        }
    }

    public string getSpawnerFromSide(string side)
    {
        switch (side)
        {
            case "top":
                return spawns_top[Random.Range(0, spawns_top.Length)].name;
            case "left":
                return spawns_left[Random.Range(0, spawns_left.Length)].name;
            case "right":
                return spawns_right[Random.Range(0, spawns_right.Length)].name;
            case "bottom":
                return spawns_bottom[Random.Range(0, spawns_bottom.Length)].name;
            default:
                return side;
        }
        
    }


    public void ChangeLayersRecursively(Transform trans, string name)
    {
        trans.gameObject.layer = LayerMask.NameToLayer(name);
        foreach (Transform child in trans)
        {
            ChangeLayersRecursively(child, name);
        }
    }

    public void ChangeSortingLayersRecursively(Transform trans, string name)
    {
        SpriteRenderer sr = trans.gameObject.GetComponent<SpriteRenderer>();
        SpriteMeshInstance sm = trans.gameObject.GetComponent<SpriteMeshInstance>();

        if (sr)
        {
            sr.sortingLayerName = name;
        } else if (sm)
        {
            sm.sortingLayerName = name;
        }

        foreach (Transform child in trans)
        {
            ChangeSortingLayersRecursively(child, name);
        }
    }

}
