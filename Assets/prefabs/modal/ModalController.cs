﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ModalController : MonoBehaviour {

    private Transform backdropTransform, textTransform;
    private CanvasRenderer backdropCanvas, textCanvas;
	public Text text;

	// Use this for initialization
	void Start () {
		text.text = "";

		backdropTransform = gameObject.transform.GetChild(0);
		backdropCanvas = backdropTransform.transform.GetComponent<CanvasRenderer>();
		backdropCanvas.SetAlpha(0f);

		textTransform = gameObject.transform.GetChild(1);
		textCanvas = textTransform.transform.GetComponent<CanvasRenderer>();
		textCanvas.SetAlpha(0f);
       
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}

	public void show(string mtext, string msubtext = "") {
		text.text = mtext;

		StartCoroutine(fadeIn(backdropCanvas, 0.5f));
		StartCoroutine(fadeIn(textCanvas));
	}


	IEnumerator fadeIn(CanvasRenderer component, float alphaPercentage = 1f, float fadeTime = 2f, bool autoFade = true)
	{
		float elapsedTime = 0f;
		float currentAlphaValue = 0f;

		while (elapsedTime < fadeTime) {
			currentAlphaValue = alphaPercentage * (elapsedTime / fadeTime);

			component.SetAlpha(currentAlphaValue);

			elapsedTime += Time.deltaTime;

			yield return null;
		}

		yield return new WaitForSeconds(3);

		if (autoFade) {
			StartCoroutine(fadeOut(backdropCanvas));
			StartCoroutine(fadeOut(textCanvas));
		}
	}

	IEnumerator fadeOut(CanvasRenderer component, float fadeTime = 2f)
	{
		float elapsedTime = 0f;
		float oldAlpha = component.GetAlpha();
		float currentAlphaValue = oldAlpha;

		while (elapsedTime < fadeTime) {
			currentAlphaValue = (1 - (elapsedTime / fadeTime)) * oldAlpha;

			component.SetAlpha(currentAlphaValue);

			elapsedTime += Time.deltaTime;

			yield return null;
		}
	}
}
