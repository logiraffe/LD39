﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour, AnimalInterface {

    public SpriteRenderer deadFish, liveFish; //Red fish, ....Jive Fish?

    public bool hasRunOutOfPower; //LOOK WE'RE USING THE THEME

    public float speed = 5.0f;
    private float swimDirX, swimDirY;

    private Color onColor = new Color(1,1,1,1);
    private Color offColor = new Color(1, 1, 1, 0);
    private float randomSpeed;
    private float offset;
    private float turnOftenness;
    private bool alive = true;

    private PlayerController player;

	// Use this for initialization
	void Start () {

        liveFish.flipX = true; //forgot to do this when making the prefabs and now I'm lazy
        deadFish.flipX = true;
        if(swimDirX == null) {
            swimDirX = 1.0f;
            swimDirY = 0.0f;
        }

        player = FindObjectOfType<PlayerController>();

        offset = Random.Range(0.0f, 0.5f);

        // Keep the fish from overlapping their glowy bits with each other
        // Gotta keep this game G rated
        //int randSort = Random.Range(10, 4000);
        //deadFish.sortingOrder = randSort;
        //liveFish.sortingOrder = randSort + 1;

        if (hasRunOutOfPower)
        {
            liveFish.color = new Color(0, 0, 0, 0);
        }
        StartCoroutine(swimMoves());
        StartCoroutine(swimForward());
        StartCoroutine(randomShimmy());
    }
	
	// Update is called once per frame
	void Update () {
        if(Mathf.Abs(this.transform.position.x - player.transform.position.x) > 30)
        {
            Destroy(this.gameObject);
        }
	}

    //-----AnimalInterface requirements----

    public void randomizeSize()
    {
        this.transform.localScale *= Random.Range(0.9f, 1.8f);
    }

    public void killMe(float lengthOfFade)
    {
        if (alive)
        {
            alive = false;
            StartCoroutine(kill(lengthOfFade));
        }
    }

    public void setSwimDirection(float x, float y)
    {
        swimDirX = x;
        swimDirY = y;
    }

    public void setSwimSpeed(float new_speed)
    {
        speed = new_speed;
    }

    public void randomize()
    {

    }

    //Should be roughly 1/2 the full animation loop
    public float spawnWait()
    {
        return 0.0f;
    }


    //--------------------------------

    public void swim()
    {
        StartCoroutine(swimMoves());
        StartCoroutine(swimForward());
        StartCoroutine(randomShimmy());
    }


    public IEnumerator kill(float deathTime)
    {
        float elapsed = 0;
        float oldSpeed = speed;

        hasRunOutOfPower = true;
        while(elapsed < deathTime)
        {
            elapsed += Time.deltaTime;
            liveFish.color = Color.Lerp(onColor, offColor, (elapsed / deathTime));
            speed = Mathf.Lerp(oldSpeed, 0.0f, (elapsed / deathTime));
            yield return null;
        }

        liveFish.color = new Color(1, 1, 1, 0); // deadFish
        
        yield return null;
    }

    public IEnumerator swimMoves()
    {
        float elapsed = 0.0f;
        float random_amount = 0.1f;
        randomSpeed = speed;

        while (true)
        {
            if(elapsed >= 1.0f)
            {
                elapsed = 0;
                random_amount = Random.Range(0.1f, 0.3f);
                StartCoroutine(lerpSpeed(randomSpeed, speed + Random.Range(0.0f, 1.0f) - 0.5f, 1.0f));

            }
            this.transform.RotateAround(Vector3.forward, Mathf.Sin(Time.time * turnOftenness + offset) * random_amount * Time.deltaTime);
            elapsed += Time.deltaTime;
            yield return null;
        }
    }

    public IEnumerator swimForward()
    {
        while (true)
        {
            this.transform.Translate(new Vector3(swimDirX, swimDirY, 0) * Time.deltaTime * speed);
            yield return null;
        }
    }

    public IEnumerator lerpSpeed(float oldSpeed, float newSpeed, float timeToLerp)
    {
        float elapsed = 0.0f;
        while(elapsed < timeToLerp)
        {
            randomSpeed = Mathf.Lerp(oldSpeed, newSpeed, (elapsed/timeToLerp));
            elapsed += Time.deltaTime;
            yield return null;
        }
    }

    public IEnumerator randomShimmy()
    {
        while (true)
        {
            if(Random.Range(0,1000) % 500 == 0)
            {
                StartCoroutine(shimmy());
            }
            yield return null;
        }
    }

    public IEnumerator shimmy()
    {
        //Store old values in atrocious names
        float oldSpeed = randomSpeed;
        float turnBack = turnOftenness;
        randomSpeed *= 3.0f;
        turnOftenness = 10;

        //For some amount of time, keep the speed boost
        float randomTime = Random.Range(0.5f, 3.0f);
        float elapsed = 0.0f;
        while(elapsed < randomTime)
        {
            elapsed += Time.deltaTime;
            yield return null;
        }

        //End the sugar rush
        randomSpeed = oldSpeed;
        turnOftenness = turnBack;
        
        yield return null;
    }
}
