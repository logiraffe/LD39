﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodController : MonoBehaviour {

	private PlayerController player;
	private float startTime;
	private float journeyLength = 0f;
	private Vector3 startPosition, endPosition;
	private float fracJourney = 0f;

	public float speed = 3F;
	public float distanceForPlayerAbsorbtion = 1f;


	void Start()
	{
		startTime = Time.time;
		startPosition = this.transform.position;
		endPosition = this.transform.position;
		player = FindObjectOfType<PlayerController>();
	}

	void Update() {

        if (player == null)
        {
            player = FindObjectOfType<PlayerController>();
        }

        float distanceFromPlayer = Vector3.Distance(startPosition, player.transform.position);

		float distCovered = (Time.time - startTime) * speed;
		if (journeyLength > 0) {
			fracJourney = distCovered / journeyLength;
		}
		if (distanceFromPlayer < 0.5f) {
			player.gainPower(20);
			Destroy(this.gameObject);
			print("HEY SHAWN, KILL THE FOOD HERE");
		} else if (distanceFromPlayer < distanceForPlayerAbsorbtion) {
			startTime = Time.time;
			startPosition = this.transform.position;
			endPosition = player.transform.position;
			endPosition.z = 0;
			journeyLength = Vector3.Distance(startPosition, endPosition);
		}

		this.transform.position = Vector3.Lerp(startPosition, endPosition, fracJourney);
	}
}
	