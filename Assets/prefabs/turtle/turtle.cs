﻿using Anima2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turtle : MonoBehaviour, AnimalInterface {

    public float swimSpeed = 1.0f;
    public float surgeSpeedIncrease = 1.0f;
    public bool swimming;

    public Sprite[] shells;

    private float elapsedTime;
    private float swimDirX = 1.0f;
    private float swimDirY = 0.0f;
    private bool surging = false;
    private bool alive;
    private bool living = true;

	// Use this for initialization
	void Start () {
        elapsedTime = 0.0f;

        //swimDirX = 1.0f;
        //swimDirY = 0.0f;

        alive = true;
        living = true;
        swimming = true;
        StartCoroutine(swimForward());

        this.transform.Find("shell").GetComponent<SpriteRenderer>().sprite = shells[Random.Range(0, shells.Length)];
	}
	
	// Update is called once per frame
	void Update () {

	}

    //-----AnimalInterface requirements----

    public void randomizeSize()
    {
        this.transform.localScale *= Random.Range(0.8f, 1.2f);
    }

    public void killMe(float lengthOfWait)
    {
        if (living)
        {
            living = false;
            StartCoroutine(waitThenFade(3.0f, lengthOfWait));
        }
    }

    public void setSwimDirection(float x, float y)
    {
        swimDirX = x;
        swimDirY = y;
    }

    public void setSwimSpeed(float speed)
    {
        swimSpeed = speed;
    }

    public void randomize()
    {

    }

    //Should be roughly 1/2 the full animation loop
    public float spawnWait()
    {
        return Random.Range(0.2f, 1.0f);
    }

    //--------------------------------

    IEnumerator waitThenFade(float fadeTime, float waitTIme)
    {
        yield return new WaitForSeconds(waitTIme);
        StartCoroutine(fadeOut(3.0f));
        StartCoroutine(ceaseTurtle(2.5f));
    }

    IEnumerator fadeOut(float fadeTime)
    {
        SpriteRenderer shell = this.transform.Find("shell").GetComponent<SpriteRenderer>();
        SpriteMeshInstance head = this.transform.Find("head").GetComponent<SpriteMeshInstance>();

        float elapsed = 0;

        while(elapsed < fadeTime)
        {
            shell.color = Color.Lerp(Color.white, Color.black, (elapsed / fadeTime));
            head.color = Color.Lerp(Color.white, Color.black, (elapsed / fadeTime));
            elapsed += Time.deltaTime;
            yield return null;
        }

        shell.color = Color.black;
        head.color = Color.black;

        yield return null;
    }

    IEnumerator ceaseTurtle(float holdLength)
    {
        //trigger death state
        this.GetComponent<Animator>().SetTrigger("death");
        
        yield return new WaitForSeconds(holdLength);

        StartCoroutine(changeSwimSpeed(0.0f, 3.0f));

        float sinkTime = 30.0f; //hehe.  It's a float
        float elapsed = 0.0f;
        float sinkSpeed = 0.0f;
        while(elapsed < sinkTime)
        {
            sinkSpeed = Mathf.Lerp(0, 0.5f, elapsed / 2.0f);
            this.transform.Translate(Vector3.down * sinkSpeed * Time.deltaTime);
            elapsed += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }

    IEnumerator swimForward()
    {
        while (swimming)
        {
            //surge on paddle downthrust at 3 seconds into animation
            if (elapsedTime >= 2.6 && !surging && alive)
            {
                surging = true;
                StartCoroutine(surge(surgeSpeedIncrease));
            }

            this.transform.Translate(new Vector3(swimDirX, swimDirY, 0) * swimSpeed * Time.deltaTime);

            //End animation loop and re-cycle at 4.05 seconds
            if (elapsedTime >= 4.05f)
            {
                elapsedTime = 0.0f;
                surging = false;
            }
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator changeSwimSpeed(float newSpeed, float changeTime = 1.0f)
    {
        float oldSpeed = swimSpeed;

        float elapsed = 0;
        while (elapsed < changeTime)
        {
            //lerp towards max surge
            swimSpeed = Mathf.Lerp(oldSpeed, newSpeed, (elapsed / changeTime));
            elapsed += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator surge(float increase)
    {
        //Surge ahead
        float surgeTime = 1.7f;
        float oldSpeed = swimSpeed;
        float newSpeed = swimSpeed + increase;

        float elapsed = 0;
        float launchTime = (surgeTime / 3) * 2;
        while (elapsed < launchTime)
        {
            //lerp towards max surge
            swimSpeed = Mathf.Lerp(oldSpeed, newSpeed, (elapsed / launchTime));
            elapsed += Time.deltaTime;
            yield return null;
        }

        swimSpeed = newSpeed;
        yield return null;

        //Immediately slow back down
        elapsed = 0;
        float brakeTime = (surgeTime / 3);
        while (elapsed < brakeTime)
        {
            //lerp back to normal speed
            swimSpeed = Mathf.Lerp(newSpeed, oldSpeed, (elapsed / brakeTime));
            elapsed += Time.deltaTime;
            yield return null;
        }

        swimSpeed = oldSpeed;
        yield return null;
    }
}
