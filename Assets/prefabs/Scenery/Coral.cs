﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coral : MonoBehaviour {

    PlayerController player;

    private float activateDist;
    private float brightness;
    private SpriteRenderer glow;

	// Use this for initialization
	void Start () {
        activateDist = 3.0f;
        player = FindObjectOfType<PlayerController>();
        brightness = 0.0f;
        glow = this.transform.GetChild(1).GetComponent<SpriteRenderer>();

    }
	
	// Update is called once per frame
	void Update () {
	    if(Mathf.Abs(player.transform.position.x - this.transform.position.x) < activateDist)
        {
            brightness = (activateDist - Mathf.Abs(player.transform.position.x - this.transform.position.x)) / activateDist;
        }
        glow.color = new Color(1, 1, 1, brightness);

	}



}
