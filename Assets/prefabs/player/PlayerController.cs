﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private float startTime;
	private float journeyLength = 0f;
	private Vector3 startPosition, endPosition;
	private float fracJourney = 0f;
	private int currentPower;
	private bool helpSent = false;
	private GameObject fish;

	public int startingPower = 50;
	public int maxPower = 100;
	public int powerDrainPerSecond = 1;
	public float speed = 3F;
	public float topSpeed = 3f;
	public float lowSpeed = 1.2f;
	public GameObject fishObject;
	public GameObject foodObject;

	// Use this for initialization
	void Start() {
		startTime = Time.time;
		startPosition = this.transform.position;
		endPosition = this.transform.position;
		currentPower = startingPower;

		StartCoroutine(drainPower());
		StartCoroutine(move());

        DontDestroyOnLoad(this.gameObject); //preserve player across scenes
    }
	
	// Update is called once per frame
	void Update() {

    }

	IEnumerator drainPower() {
		bool wait = false;
		int fadeFlag = 1;

		while (true) {
			// Start removing speed after 20 health
//			if (this.currentPower <= 20) {
//				this.speed = this.lowSpeed + (this.topSpeed * (this.currentPower / this.maxPower));
//			}

			if (this.currentPower >= 10) {
				print("power level: " + this.currentPower);
				this.currentPower -= this.powerDrainPerSecond;
				if (this.currentPower < 25 && fadeFlag == 4) {
					FindObjectOfType<PlayerAesthetics>().fadeTo(2.5f, 2f);
					print("fade " + fadeFlag);
				} 
				else if (this.currentPower < 50 && fadeFlag == 3) {
					FindObjectOfType<PlayerAesthetics>().fadeTo(5f, 2f);
					print("fade " + fadeFlag);
					fadeFlag = 4;
				}
				else if (this.currentPower < 75 && fadeFlag == 2) {
					FindObjectOfType<PlayerAesthetics>().fadeTo(7.5f, 2f);
					print("fade " + fadeFlag);
					fadeFlag = 3;
				}
				else if (this.currentPower < 100 && fadeFlag == 1) {
					FindObjectOfType<PlayerAesthetics>().fadeTo(10f, 2f);
					print("fade " + fadeFlag);
					fadeFlag = 2;
				}
			} else if (!helpSent && !wait) {
				StartCoroutine(sendHelp());
				wait = true;
			} else if (helpSent) {
				wait = false;
//				print("help was sent and you have food");
			}

			yield return new WaitForSeconds(5);
		}
	}

	IEnumerator sendHelp() {
//		print("halp fish: " + this.currentPower);
		float spawnX = transform.position.x - 10f;
		float spawnY = transform.position.y * -1f;
		Vector3 helpSpawn = new Vector3(spawnX, spawnY);
		fish = Instantiate(fishObject, helpSpawn, Quaternion.identity);
		fish.GetComponent<AnimalInterface>().randomizeSize();
		fish.GetComponent<AnimalInterface>().setSwimSpeed(1f);
		fish.GetComponent<AnimalInterface>().setSwimDirection(transform.position.x + 10f, transform.position.y);

		while (!helpSent) {
			if (fish.transform.position.x > transform.position.x + 1f) {
				GameObject food = Instantiate(foodObject, fish.transform.position, Quaternion.identity);
				helpSent = true;
				//print("help sent");
				break;
			}

			yield return new WaitForSeconds(0.5f);
		}
		
	}

	IEnumerator move() {
        yield return null;
		// Move with click
		while (true) {
			float distCovered = (Time.time - startTime) * speed;
			if (journeyLength > 0) {
				fracJourney = distCovered / journeyLength;
			}
			
			this.transform.position = Vector3.Lerp(startPosition, endPosition, fracJourney);
			
			if (Input.GetButton("Fire1")) {
				startTime = Time.time;
				startPosition = this.transform.position;
				endPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				endPosition.z = 0;
				journeyLength = Vector3.Distance(startPosition, endPosition);
			}
			yield return null;
		}

		// Mouse monitor moving method
//		while (true) {
//			float distCovered = (Time.time - startTime) * speed;
//			if (journeyLength > 0) {
//				fracJourney = (distCovered / journeyLength);
//			}
//
//			this.transform.position = Vector3.Lerp(startPosition, endPosition, fracJourney);
//			startTime = Time.time;
//
//			yield return null;
//
//			startPosition = this.transform.position;
//			endPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
//			endPosition.z = 0;
//			journeyLength = Vector3.Distance(startPosition, endPosition);
//
//		}
	}

	public void gainPower(int power) {
		this.currentPower += power;
        FindObjectOfType<sound_manager>().playFood();
        FindObjectOfType<PlayerAesthetics>().eatFood();
	}
}

