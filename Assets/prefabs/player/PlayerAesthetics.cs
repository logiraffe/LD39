﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAesthetics : MonoBehaviour {

    public ParticleSystem light;
    public ParticleSystem core;
    public ParticleSystem happy_burst;

    private float full_light_emit_rate = 8;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void eatFood()
    {
        happy_burst.Play();
    }

    public void fadeTo(float percentage, float time)
    {
        StartCoroutine(fadePlayer(percentage, time));
        StartCoroutine(fadeLight(percentage, time / 2));
    }

    private IEnumerator fadePlayer(float amount, float time_to_fade)
    {
        float elapsed = 0;
        float core_brightness = core.emission.rateMultiplier;
        ParticleSystem.EmissionModule temp_core = core.emission;

        while (elapsed < time_to_fade)
        {
            temp_core.rateMultiplier = Mathf.Lerp(core_brightness, amount, (elapsed / time_to_fade));
            elapsed += Time.deltaTime;
            yield return null;
        }
        
        temp_core.rateMultiplier = amount;

        yield return null;
    }

    private IEnumerator fadeLight(float amount, float time_to_fade)
    {
        float elapsed = 0;
        float current_brightness = light.emission.rateMultiplier;
        ParticleSystem.EmissionModule temp_light = light.emission;

        while (elapsed < time_to_fade)
        {
            temp_light.rateMultiplier = Mathf.Lerp(current_brightness, amount, (elapsed / time_to_fade));
            elapsed += Time.deltaTime;
            yield return null;
        }

        temp_light.rateMultiplier = amount;

        yield return null;
    }


}
