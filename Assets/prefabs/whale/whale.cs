﻿using Anima2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class whale : MonoBehaviour, AnimalInterface {

    public float swimSpeed = 1.0f;
    float swimDirX, swimDirY;
    bool alive, swimming;

	// Use this for initialization
	void Start () {
        alive = true;
        swimming = true;
        if (swimDirX == null)
        {
            swimDirX = 1.0f;
            swimDirY = 0.0f;
        }

        StartCoroutine(swim());
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    //-----AnimalInterface requirements----

    public void randomizeSize()
    {
        this.transform.localScale *= Random.Range(1.0f, 1.4f);
    }

    public void killMe(float lengthOfFade = 1.0f)
    {
        if (alive)
        {
            alive = false;
            fadeOut(lengthOfFade);
            //death animation
            this.gameObject.GetComponent<Animator>().SetTrigger("death");
            StartCoroutine(stopSwimming(6.0f));
        }
    }

    public void setSwimDirection(float x, float y)
    {
        swimDirX = x;
        swimDirY = y;
    }

    public void setSwimSpeed(float speed)
    {
        swimSpeed = speed;
    }

    public void randomize()
    {
        
    }

    //Should be roughly 1/2 the full animation loop
    public float spawnWait()
    {
        return Random.Range(0.3f, 2.0f);
    }

    //--------------------------------

    private IEnumerator stopSwimming(float time)
    {
        float oldSpeed = swimSpeed;
        float elap = 0.0f;
        while(elap < time)
        {
            swimSpeed = Mathf.Lerp(oldSpeed, 0.0f, (elap / time));
            elap += Time.deltaTime;
            yield return null;
        }
        yield return null;
    }

    private void fadeOut(float lengthOfFade)
    {
        foreach(Transform child in this.transform)
        {
            SpriteMeshInstance sm = child.GetComponent<SpriteMeshInstance>();
            SpriteRenderer sr = child.GetComponent<SpriteRenderer>();

            if (sm)
            {
                StartCoroutine(fadeSpriteMeshOut(lengthOfFade, sm));
            } else if (sr)
            {
                StartCoroutine(fadeSpriteOut(lengthOfFade, sr));
            }
            else {
                //should cover it
            }
        }
    }
    

    IEnumerator fadeSpriteMeshOut(float fadeTime, SpriteMeshInstance piece)
    {
        float elapsed = 0;
        while (elapsed < fadeTime)
        {
            piece.color = Color.Lerp(Color.white, Color.black, (elapsed / fadeTime));
            elapsed += Time.deltaTime;
            yield return null;
        }

        piece.color = Color.black;

        yield return null;
    }

    IEnumerator fadeSpriteOut(float fadeTime, SpriteRenderer piece)
    {
        float elapsed = 0;
        while (elapsed < fadeTime)
        {
            piece.color = Color.Lerp(Color.white, Color.black, (elapsed / fadeTime));
            elapsed += Time.deltaTime;
            yield return null;
        }

        piece.color = Color.black;

        yield return null;
    }

    IEnumerator swim()
    {
        while (swimming)
        {
            this.transform.Translate(new Vector3(swimDirX, swimDirY, 0) * swimSpeed * Time.deltaTime);
            yield return null;
        }
    }


}
