﻿using Anima2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ray : MonoBehaviour, AnimalInterface {

    public Animator anim;
    public Transform head;

    private float tiltTime = 1.0f;
    public float swimSpeed = 5.0f;
    public float swimDirX = 1.0f;
    public float swimDirY = -0.1f;
    public bool swimming;

    private PlayerController pc;
    private bool living = true;

    // Use this for initialization
    void Start () {
        swimming = true;
        living = true;
        StartCoroutine(swim());
        pc = FindObjectOfType<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        //if(pc.transform)
        //wooshing noise here

        
	}

    //-----AnimalInterface requirements----

    public void randomizeSize()
    {
        this.transform.localScale *= Random.Range(0.5f, 1.5f);
    }

    public void killMe(float lengthOfWait)
    {
        if (living)
        {
            living = false;
            StartCoroutine(waitThenFade(3.0f, lengthOfWait));
        }
    }

    public void setSwimDirection(float x, float y)
    {
        swimDirX = x;
        swimDirY = y;
    }

    public void setSwimSpeed(float speed)
    {
        swimSpeed = speed;
    }

    public void randomize()
    {

    }

    //Should be roughly 1/2 the full animation loop
    public float spawnWait()
    {
        return Random.Range(0.2f, 1.0f);
    }

    //--------------------------------

    IEnumerator swimUp(float degreesUp)
    {
        yield return tiltHead(degreesUp);
        //lerp head up

        //lerp body up & head down

        yield return tiltHead(degreesUp);

        yield return null;
    }

    IEnumerator tiltHead(float degreesUp)
    {
        float elapsed = 0.0f;
        Quaternion startRot, endRot;
        startRot = head.rotation;
        endRot = Quaternion.Euler(head.rotation.eulerAngles.x, head.rotation.eulerAngles.y, head.rotation.eulerAngles.x);
        while(elapsed < tiltTime)
        {
            elapsed += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }

    IEnumerator swim()
    {
        while (swimming)
        {
            this.transform.Translate(new Vector3(swimDirX, swimDirY, 0) * swimSpeed * Time.deltaTime);
            yield return null;
        }
    }

    IEnumerator waitThenFade(float fadeTime, float waitTIme)
    {
        yield return new WaitForSeconds(waitTIme);
        StartCoroutine(fadeOut(3.0f));
        StartCoroutine(ceaseTurtle(1.5f));
    }

    IEnumerator fadeOut(float fadeTime)
    {
        SpriteMeshInstance piece;


        float elapsed = 0;

        while (elapsed < fadeTime)
        {
            foreach(Transform child in this.transform)
            {
                if (!child.gameObject.active)
                {
                    continue;
                }
                piece = child.GetComponent<SpriteMeshInstance>();
                if (piece != null)
                {
                    piece.color = Color.Lerp(Color.white, Color.black, (elapsed / fadeTime));
                }
            }
            elapsed += Time.deltaTime;
            yield return null;
        }

        foreach (Transform child in this.transform)
        {
            piece = child.GetComponent<SpriteMeshInstance>();
            if (piece != null)
            {
                piece.color = Color.black;
            }
        }


        yield return null;
    }

    IEnumerator ceaseTurtle(float holdLength)
    {
        //trigger death state
        this.GetComponent<Animator>().SetTrigger("death");

        yield return new WaitForSeconds(holdLength);

        StartCoroutine(changeSwimSpeed(0.0f, 3.0f));

        float sinkTime = 30.0f; //hehe.  It's a float
        float elapsed = 0.0f;
        float sinkSpeed = 0.0f;
        while (elapsed < sinkTime)
        {
            sinkSpeed = Mathf.Lerp(0, 0.5f, elapsed / 2.0f);
            this.transform.Translate(Vector3.down * sinkSpeed * Time.deltaTime);
            elapsed += Time.deltaTime;
            yield return null;
        }

        yield return null;
    }

    IEnumerator changeSwimSpeed(float newSpeed, float changeTime = 1.0f)
    {
        float oldSpeed = swimSpeed;

        float elapsed = 0;
        while (elapsed < changeTime)
        {
            //lerp towards max surge
            swimSpeed = Mathf.Lerp(oldSpeed, newSpeed, (elapsed / changeTime));
            elapsed += Time.deltaTime;
            yield return null;
        }
    }
}
